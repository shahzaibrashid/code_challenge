from django import forms

__author__ = 'srashid'


class ProfileForm(forms.Form):
    first_name = forms.CharField(max_length=64)
    last_name = forms.CharField(max_length=64)
    age = forms.IntegerField(min_value=0)