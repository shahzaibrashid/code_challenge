from django.conf.urls import patterns, url
from task.views import profile_input

__author__ = 'srashid'


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'code_challenge.views.home', name='home'),
    url(r'^$', profile_input, name='profile'),
)
