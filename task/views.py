from django.core.context_processors import csrf
from django.forms.formsets import formset_factory
from django.http import HttpResponse
from django.shortcuts import render, render_to_response

# Create your views here.
from django.template import RequestContext
from task.forms import ProfileForm


def sort_key(person):
    last_name = person['last_name']
    middle_index = len(last_name) / 2
    return last_name[middle_index]


def profile_input(request):
    profile_formset = formset_factory(ProfileForm, extra=5)
    # form = ProfileForm(request.POST or None)
    if request.POST:
        formset = profile_formset(request.POST)
        if formset.is_valid():
            output = ''
            profile_list = list()
            for form in formset:
                profile_list.append(form.cleaned_data)
            sorted_profiles = sorted(profile_list, key=sort_key)
            return render_to_response('task/profile_response.html', {'resp': sorted_profiles}, context_instance=RequestContext(request))
    else:
        formset = profile_formset()
    args = dict()
    args.update(csrf(request))
    args['formset'] = formset
    return render_to_response('task/profile.html', args, context_instance=RequestContext(request))

